#!/usr/bin/python
#Gets the top magnet link for a file from piratebay and stores it with its title
import argparse
from torrent_retriever import TorrentRetriever

parser = argparse.ArgumentParser(description='Gets the top magnet link for a file from piratebay and stores it with its title')
parser.add_argument('file_name', metavar='N', type=str, nargs='+',help='file name, will always be considered one file name despite being separated by spaces')
parser.add_argument("-a","--auto",action='store_true' ,help="Download torrent and add to rtorrent watch directory automatically")

args = parser.parse_args()

tr = TorrentRetriever()
tr.main(args.file_name, args.auto)
