from bs4 import BeautifulSoup
from lxml import html
import requests
import os

class TorrentRetriever:
    MIN_SEEDERS = 100
    DIRECTORY = '/dev/shm/'
    RTORRENT_WATCH_FOLDER='/media/flash_drive/torrent/watch/'

    def __init__(self, rWatchFolder=RTORRENT_WATCH_FOLDER):
        self.RTORRENT_WATCH_FOLDER = rWatchFolder

    def getTorrentPage(self, name):
        """ Gets the html page for the file listed in 'name', sorted by seeds """
        print "Get page"
        #url = "http://thepiratebay.se/search/"+name+"/0/7/0"
        #url = "https://kickass.unblocked.pw/usearch/"+name+"/?field=seeders&sorder=desc"
        url = "https://kat.cr/usearch/"+name+"/?field=seeders&sorder=desc"
        #sock = urllib.urlopen(url)
        page = requests.get(url)
        htmlSource = page.text
        return htmlSource

    def getMagnetLinkText(self, soup):
        """ Given a piratebay html page, returns the first magnet link """
        magnet_link = soup.find(attrs={"class" : "icon16", "title":"Torrent magnet link"}).attrs.get('href')
        return magnet_link

    def areSeedersAdequate(self, soup, minSeeders):
        """ Given a piratebay html file, determines the number of seeds for the top torrent """
        currentSeeders = self.getSeederNumber(soup)
        if currentSeeders < minSeeders:
            return False
        else:
            return True
        
    def getSeederNumber(self, soup):
        #currentSeeders = int(soup.find(attrs={"align" : "right"}).text)
        currentSeeders = int(soup.find(attrs={"class" : "green center"}).text)
        return currentSeeders

    def getLeecherNumber(self, soup):
        currentLeechers = int(soup.find(attrs={"class" : "red lasttd center"}).text)
        return currentLeechers

    def getTorrentAge(self, soup):
        # Get the seeders node and return the text of the node before that.
        uploadAge = soup.find(attrs={"class" : "green center"}).findPreviousSibling().text.encode('ascii','ignore')
        return uploadAge

    def storeLinkAt(self, filePath, magnet_link):
        """ Given a filepath, preferably empty, stores the magnet link there """
        f = open(filePath, 'w')
        f.write(magnet_link)
        f.close()
        print("Magnet link written to: " + filePath)

    def printFileInfo(self, soup):
        title = self.getTitle(soup)
        seeders = self.getSeederNumber(soup)
        leechers = self.getLeecherNumber(soup)
        size = self.getSize(soup)
        date = self.getTorrentAge(soup)
        uploader = soup.find(attrs={"class" : "font11px lightgrey block"}).find(attrs={"class" : "plain"}).text
        print(title)
        print("Seeders: " + str(seeders) + ", Leechers: " + str(leechers))
        print("Uploader: " + uploader)
        print("Age: " + date)
        print("Size: " + size)

    def getSize(self, soup):
        return soup.find(attrs={"class" : "nobr center"}).text.encode('ascii','ignore')
        
    def getTitle(self, soup):
      return soup.find(attrs={"class" : "cellMainLink"}).text.encode('ascii','ignore')

    def getFileNameToStore(self, soup):
      return self.getTitle(soup).replace(' ','_').replace(os.sep,"")+'.meta'

    def foundResults(self, soup):
        """ Checks if there were no results"""
        return soup.find(attrs={"class" : "markeredBlock torType filmType"}) != None

    def getSoup(self, name):
        print("Name is: " + name)
        #Get the html for the filename
        html = self.getTorrentPage(name)
        #Get soup for the html
        soup = BeautifulSoup(html)
        return soup

    def storeLink(self, soup):
        magnet_link = self.getMagnetLinkText(soup)
        self.storeLinkAt(self.DIRECTORY+getFileNameToStore(soup), magnet_link)
        print("")
        print(magnet_link)

    def encodeForRtorrentWatchFolder(self, magnet_link):
        num = len(magnet_link)
        magnet_link="d10:magnet-uri"+str(num)+":"+magnet_link+'e'
        return magnet_link

    def addToRtorrent(self, soup):
        pathToStoreFile = self.RTORRENT_WATCH_FOLDER+self.getFileNameToStore(soup);
        self.storeLinkAt(pathToStoreFile, self.encodeForRtorrentWatchFolder(self.getMagnetLinkText(soup)))
        return pathToStoreFile

    def storeOptions(self, soup):
        if raw_input("Add to rtorrent? Will just store in a file otherwise y/n") == "y":
            return self.addToRtorrent(soup)
        else:
            self.storeLink(soup)

    def main(self, file_name, auto):
        try:
            # join all space separated elements into one 
            name = ' '.join(file_name)
            soup = self.getSoup(name)
            storedPath = ""

            if self.foundResults(soup):
                self.printFileInfo(soup)
                if(auto):
                    storedPath = self.addToRtorrent(soup)
                    print("Auto mode, file added")
                    return storedPath

                if(self.areSeedersAdequate(soup, self.MIN_SEEDERS)):
                    storedPath = self.storeOptions(soup)
                    return storedPath
                else:
                    print("Warning, there are only " + str(self.getSeederNumber(soup)) + " seeders.")
                    if raw_input("Do you want to continue? y/n") == "y":
                        storedPath = self.storeOptions(soup)
                        return storedPath
                    else:
                        print("Stopping")
            else:
                print("No results found")
        except KeyboardInterrupt:
            print("\nExiting")

